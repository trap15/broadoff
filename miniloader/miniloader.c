/*
        BroadOFF - A BootMii/PPC ELF loader. (Reloader stub)
        Requires mini.

Copyright (C) 2008, 2009        Haxx Enterprises <bushing@gmail.com>
Copyright (C) 2009              Andre Heider "dhewg" <dhewg@wiibrew.org>
Copyright (C) 2008, 2009        Hector Martin "marcan" <marcan@marcansoft.com>
Copyright (C) 2008, 2009        Sven Peter <svenpeter@gmail.com>
Copyright (C) 2009              John Kelley <wiidev@kelley.ca>
Copyright (C) 2009-2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <types.h>
#include <string.h>
#include <ipc_common.h>

typedef struct {
	u32 code;
	u32 tag;
	u32 args[6];
} ipc_request;

static volatile ipc_request *in_queue;
static volatile ipc_request *out_queue;

static int in_size;
static int out_size;

static int initialized = 0;

static u16 out_head;
static u16 in_tail;

typedef const struct {
	char magic[3];
	char version;
	void *mem2_boundary;
	volatile ipc_request *ipc_in;
	u32 ipc_in_size;
	volatile ipc_request *ipc_out;
	u32 ipc_out_size;
} ipc_infohdr;

static ipc_infohdr *infohdr;

static u32 cur_tag;

#define		virt_to_phys(x)		(((u32)(x)) & 0x3FFFFFFF)
#define		phys_to_virt(x)		(((u32)(x)) | 0x80000000)

#define		HW_REG_BASE			0xD800000			/*!< Base for most hardware */
#define		HW_IPC_PPCMSG		(HW_REG_BASE + 0x000) //PPC to ARM
#define		HW_IPC_PPCCTRL		(HW_REG_BASE + 0x004)
#define		HW_IPC_ARMMSG		(HW_REG_BASE + 0x008) //ARM to PPC
#define		HW_GPIO1OUT		(HW_REG_BASE + 0x0E0)

#define		IPC_CTRL_SEND		0x01
// Set by peer to acknowledge a message. Write one to clear.
#define		IPC_CTRL_SENT		0x02
// Set by peer to send a message. Write one to clear.
#define		IPC_CTRL_RECV		0x04
// Write one acknowledge a message. Cleared when peer writes one to IPC_CTRL_SENT.
#define		IPC_CTRL_RECVD		0x08
// Enable interrupt when a message is received
#define		IPC_CTRL_INT_RECV	0x10
// Enable interrupt when a sent message is acknowledged
#define		IPC_CTRL_INT_SENT	0x20

static inline u32 read32(u32 addr)
{
	u32 x;
	asm volatile("lwz %0,0(%1) ; sync" : "=r"(x) : "b"(0xC0000000 | addr));
	return x;
}

static inline void write32(u32 addr, u32 x)
{
	asm("stw %0,0(%1) ; eieio" : : "r"(x), "b"(0xC0000000 | addr));
}

static inline void set32(u32 addr, u32 set)
{
	write32(addr, read32(addr) | set);
}

static inline void mask32(u32 addr, u32 clear, u32 set)
{
	write32(addr, (read32(addr) & (~clear)) | set);
}

void ppcsync()
{
	asm("sync ; isync");
}

void sync_before_read(void *p, u32 len)
{
	u32 a, b;
	a = (u32)p & ~0x1F;
	b = ((u32)p + len + 0x1F) & ~0x1F;
	for(; a < b; a += 32)
		asm("dcbi 0,%0" : : "b"(a));
	
	ppcsync();
}

void sync_after_write(const void *p, u32 len)
{
	u32 a, b;
	a = (u32)p & ~0x1F;
	b = ((u32)p + len + 0x1F) & ~0x1F;
	for(; a < b; a += 32)
		asm("dcbst 0,%0" : : "b"(a));
	
	ppcsync();
}

void *memset(void *b, int c, size_t len)
{
	size_t i;
	for(i = 0; i < len; i++)
		((u8*)b)[i] = c;
	return b;
}


static inline u16 peek_outtail(void)
{
	return read32(HW_IPC_ARMMSG) & 0xFFFF;
}
static inline u16 peek_inhead(void)
{
	return read32(HW_IPC_ARMMSG) >> 16;
}

static inline void poke_intail(u16 num)
{
	mask32(HW_IPC_PPCMSG, 0xFFFF, num);
}
static inline void poke_outhead(u16 num)
{
	mask32(HW_IPC_PPCMSG, 0xFFFF0000, num << 16);
}

void ipc_flush(void)
{
	int n = 0;
	if(!initialized) {
		return;
	}
	while(peek_inhead() != in_tail) {
		if(n++ > 20000) {
			n = 0;
		}
	}
}

int ipc_initialize(void)
{
	
	infohdr = (ipc_infohdr*)(phys_to_virt(read32(0x13FFFFFC)));
	sync_before_read((void*)infohdr, sizeof(ipc_infohdr));
	
/*	if((infohdr->magic[0] != 'I') || (infohdr->magic[1] != 'P') || (infohdr->magic[2] == 'C')) {
		return 0;
	}
	if(infohdr->version != 1) {
		return 0;
	}*/
	
	in_queue = (ipc_request *)phys_to_virt((u32)infohdr->ipc_in);
	out_queue = (ipc_request *)phys_to_virt((u32)infohdr->ipc_out);
	
	in_size = infohdr->ipc_in_size;
	out_size = infohdr->ipc_out_size;
	
	in_tail = read32(HW_IPC_PPCMSG) & 0xFFFF;
	out_head = read32(HW_IPC_PPCMSG) >> 16;
	
	cur_tag = 1;
	
	initialized = 1;
	return 1;
}

void ipc_shutdown(void)
{
	if(!initialized)
		return;
	ipc_flush();
	initialized = 0;
}

void ipc_post(u32 code, u32 tag, u32 num_args, u32* args)
{
	int arg = 0;
	int n = 0;
	
	if(!initialized) {
		return;
	}
	
	if(peek_inhead() == ((in_tail + 1)&(in_size-1))) {
		while(peek_inhead() == ((in_tail + 1)&(in_size-1))) {
			if(n++ > 20000) {
				n = 0;
			}
		}
	}
	in_queue[in_tail].code = code;
	in_queue[in_tail].tag = tag;
	while(num_args--) {
		in_queue[in_tail].args[arg] = args[arg];
		arg++;
	}
	sync_after_write((void*)&in_queue[in_tail], 32);
	in_tail = (in_tail+1)&(in_size-1);
	poke_intail(in_tail);
	write32(HW_IPC_PPCCTRL, IPC_CTRL_SEND);
}

// last IPC message received, copied because we need to make space in the queue
ipc_request req_recv;

// since we're not using IRQs, we don't use the reception bell at all at the moment
ipc_request *ipc_receive(void)
{
	while(peek_outtail() == out_head);
	sync_before_read((void*)&out_queue[out_head], 32);
	req_recv = out_queue[out_head];
	out_head = (out_head+1)&(out_size-1);
	poke_outhead(out_head);
	
	return &req_recv;
}

void ipc_process_unhandled(volatile ipc_request *rep)
{
	(void)rep;
}

ipc_request *ipc_receive_tagged(u32 code, u32 tag)
{
	ipc_request *rep;
	rep = ipc_receive();
	while(rep->code != code || rep->tag != tag) {
		ipc_process_unhandled(rep);
		rep = ipc_receive();
	}
	return rep;
}

ipc_request *ipc_exchange(u32 code, u32 num_args, void* args)
{
	ipc_request *rep;
	
	ipc_post(code, cur_tag, num_args, args);
	
	rep = ipc_receive_tagged(code, cur_tag);
	
	cur_tag++;
	return rep;
}

static inline void ipc_slowping(void)
{
	ipc_exchange(IPC_CODE(IPC_SLOW, IPC_DEV_SYS, IPC_SYS_PING), 0, NULL);
}

int ipc_powerpc_boot_file(const char *file)
{
	ipc_request *req;
	void* array[2];
	array[0] = 0;
	array[1] = (void*)virt_to_phys(file);
	sync_after_write(file, 24);
	req = ipc_exchange(IPC_CODE(IPC_SLOW, IPC_DEV_PPC, IPC_PPC_BOOT_FILE), 2, array);
	return req->args[0];
}

/* To chop down on space, let's use this as main */
int cstart(void)
{
	char pathptr[] = "0:/bootmii/ppcboot.elf";
	int vector = 0;
	for(vector = 0x100; vector < 0x2000; vector += 0x10) {
		u32 *insn = (u32 *)(0x80000000 + vector);
		insn[0] = 0x4C000064;			// rfi
		insn[1] = 0;
	}
	if(!ipc_initialize())
		set32(HW_GPIO1OUT, 2);
	ipc_slowping();
	ipc_powerpc_boot_file(pathptr);
	
	set32(HW_GPIO1OUT, 2);
	for(;;);
	
	return 0;
}

