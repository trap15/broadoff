ifeq ($(strip $(WIIDEV)),)
$(error "Set WIIDEV in your environment.")
endif

include $(WIIDEV)/broadway.mk

LIBS += -ldiskmii
LIBS += -lbroadway
CFLAGS += -O3

TARGET = ppcboot.elf

OBJS = miniload.o main.o

include $(WIIDEV)/common.mk

%.o: %.bin
	$(bin2o)

